import app from '../../src/index';
import supertest from 'supertest';
import DoneCallback = jest.DoneCallback;
const request = supertest(app);

describe("/todos : Todo list", () => {
    test("Return 200", async () => {
        const response = await request.get('/todos');
        expect(response.statusCode).toBe(200);
    });
})

describe("/todos/:2 : testing element", () => {
    test("Return 200", async () => {
        const response = await request.get('/todos/:2');
        expect(response.statusCode).toBe(200);
    });
})

describe("Unknow endpoint", () => {
    test("Return 404", async () => {
        const response = await request.get("/unknow")
        expect(response.status).toBe(404)
    });
    test("Return not found message", async () => {
        const response = await request.get("/unknow")
        expect(response.body.error).toBeDefined();
        expect(response.body.error).toBe("Route not found");
    });
});

afterAll((done: DoneCallback) => {
    done();
});