// j'ai mis du JSON parce que c'est dimanche mais j'ai bien compris que c'est
// ici qu'on fait la connexion avec la database. D'ailleurs tout est donné dans
// le sujet de l'epytodo (mySQL_HOST, mySQL_USER, ...)

export interface User {
    id: number;
    email: string;
    password: string;
    name: string;
    first: string;
    created_at: Date
}

export interface Todo {
    id: number;
    title: string;
    description: string;
    created_at: Date;
    due_time: Date;
    status: string;
    user_id: number
}