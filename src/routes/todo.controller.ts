import { Router } from 'express';
import { NextFunction, Request, Response } from 'express';
import { TodoService } from './todo.service';

const TodoController: Router = Router();
const Todo = new TodoService();

TodoController.get('/', (req: Request, res: Response, next: NextFunction) => {
    try {
        res.json(Todo.getTodo());
    } catch (err) {
        next(err);
    }
});

TodoController.get('/:id', (req: Request, res: Response, next: NextFunction) => {
    const str = req.params.id.match(/\d+/);
    const id = Number(str);

    if (isNaN(id))
        return res.status(500).json({ error: 'Id in URL is not a number' })
    try {
        res.json(Todo.getTodoId(id));
    } catch (err) {
        next(err);
    }
});

TodoController.post('/', (req: Request, res: Response, next: NextFunction) => {
    try {
        res.json(Todo.create_element(req.body));
    } catch (err) {
        next(err);
    }
});

TodoController.put('/:id', (req: Request, res: Response, next: NextFunction) => {
    const str = req.params.id.match(/\d+/);
    const id: number = Number(str);

    if (isNaN(id))
        return res.status(500).json({ error: 'Id in URL is not a number' })
    try {
        res.json(Todo.update_element(id, req.body));
    } catch (err) {
        next(err);
    }
});

TodoController.delete('/:id', (req: Request, res: Response, next: NextFunction) => {
    const str = req.params.id.match(/\d+/);
    const id: number = Number(str);

    if (isNaN(id))
        return res.status(500).json({ error: 'Id in URL is not a number' })
    try {
        res.json(Todo.delete_element(id));
    } catch (err) {
        next(err);
    }
});

export default TodoController;