import { Router } from 'express';
import { NextFunction, Request, Response } from 'express';
import { Todo } from '../config_types/data';
import { readFileSync } from 'fs';
import todo from '../../data/todo.json';

export class TodoService {
    public getTodo(): any {
        return todo;
    }

    public getTodoId(id : number): Array<any> {
        const t: Array<Todo> = this.getTodo();
        const item: Array<Todo> = t.filter((i) => i.id === id);

        // Je voulais ajouter une gestion d'erreur pour les id inconnu mais pas réussi
        return item;
    }

    public create_element(body: Array<any>): Array<any> {
        const t: Array<Todo> = this.getTodo();
        let element: Array<any> = body;
        let i: number = 0;
        let str: string = '';
        let result: Array<Todo> = [];

        //insérer str à element pour ajouter l'id
        t.forEach(tab => i++);
        i++;
        str = `"i": ${i}`;

        result = t.concat(element);
        console.log(result);
        return result;
    }

    public update_element(id: number, body: Array<any>): Array<any> {
        const t: Array<Todo> = this.getTodo();
        const item: Array<Todo> = t.filter((i) => i.id === id);
        let element: Array<any> = body

        // remplacer dans la base de données item par element
        return element;
    }

    public delete_element(id: number): any {
        const t: Array<Todo> = this.getTodo();
        const item: Array<Todo> = t.filter((i) => i.id === id);

        // chercher l'item en question et le supprimer de la base de données
        return `"msg" : "Successfully deleted record number : ${id}"`
    }
}