// Après avoir lu le sujet, j'ai décidé de faire la partie todo. Elle permet
// de créer une route GET POST PUT DELETE.. Comme ce que tu m'as demandé..

import express from 'express';
import bodyParser from 'body-parser';
import { UnknownRoutesHandler } from './middleware/unknowRoutes.handler';
import { ExceptionsHandler } from './middleware/exceptions.handler';
import TodoController from './routes/todo.controller';

const app = express();

app.use(bodyParser.json());
app.get('/', (req, res) => {
    res.send('par dessous les faguots')
});
app.use('/todos', TodoController);
app.all('*', UnknownRoutesHandler);
app.use(ExceptionsHandler);
app.listen(3000, () => console.log('App listening on port 3000'));
export default app;