import type { Config } from '@jest/types';

const jestConfig : Config.InitialOptions = {
    transform: {
        '^.+\\.tsx?$': 'ts-jest',
        },
    testEnvironment: 'node',
    verbose: true,
    forceExit: true,
    collectCoverage: true,
    resetMocks: true
}

export default jestConfig;